//
//  ViewController.m
//  appspotrChallenge
//
//  Created by Tobias Ednersson on 30/03/16.
//  Copyright © 2016 Tobias Ednersson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textview;
@property NSString * urlstring;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.urlstring = @"https://api2.appspotr.com/givemeachallenge";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL * url = [[NSURL alloc] initWithString:self.urlstring] ;
    
    [self.textview setTextAlignment:NSTextAlignmentCenter];
    
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                
                
                if(error) {
                    NSLog(@"Something went wrong");
                    return;
                }
                
                NSError * jsonerror;
                
                NSDictionary * dict = [NSJSONSerialization
                             JSONObjectWithData:data
                             options:0
                             error:&jsonerror];
            
                
                if(jsonerror) {
                    NSLog(@"Json Parse Error");
                    return;
                }
                
                NSArray * singles = [self filterSingles:dict[@"quiz"]];
    
                dispatch_async(dispatch_get_main_queue(), ^() {
                    
                    self.titleLabel.text = dict[@"question"];
                    //expect one word....
                    self.textview.text = [NSString stringWithFormat:@"The word is %@",singles[0]];
                });
                
                
            }] resume];
    
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

      
      
      
      - (NSArray *) filterSingles: (NSArray *) words {

    NSMutableDictionary *doubles = [[NSMutableDictionary alloc] init];
    for(NSString * word in words) {
        
        if(!doubles[word])
        {
            doubles[word] = [NSNumber numberWithInt:1];
        
        }
        
        else {

            NSNumber * value = doubles[word];
            NSNumber * newValue = [NSNumber numberWithInt:value.intValue + 1];
            doubles[word] = newValue;

        }
        
    }

    NSMutableArray * filtered = [[NSMutableArray alloc] init];
    
        for(NSString * key in [doubles allKeys]) {
            
         NSNumber * num = (NSNumber *) doubles[key];
        
            if(num.intValue == 1) {
                [filtered addObject:key];
            }
        }
    
          for(NSString * word in filtered) {
              NSLog(@"The word is: %@",word);
          }
    return [filtered copy];
}


@end
